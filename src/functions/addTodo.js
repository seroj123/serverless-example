"use strict";

const { v4 } = require('uuid');
const AWS = require('aws-sdk');
const sns = new AWS.SNS();
const middy = require('@middy/core');
const httpJsonBodyParser = require('@middy/http-json-body-parser');

const addTodo = async (event) => {
  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const { todo } = event.body;
  const createdAt = new Date().toISOString();
  const id = v4();

  const newTodo = {
    id,
    todo,
    createdAt,
    completed: false
  };

  await dynamodb
  .put({
    TableName: 'TodoTable',
    Item: newTodo
  }).promise();

console.log('Before sent ================');
  await sns
  .publish({
    Message: `${id}`,
    TopicArn: 'arn:aws:sns:eu-west-3:340264908487:todo-created'
  })
  .promise();
  console.log('After sent ======================');

  return {
    statusCode: 200,
    body: JSON.stringify(newTodo),
  };
};

module.exports = {
  handler: middy(addTodo).use(httpJsonBodyParser())
};
